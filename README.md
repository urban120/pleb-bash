# README #

To je program za preverjanje domačih nalog v C-ju.
Program poleg izhoda v terminalu zgenerira še spletno stran

## Uporaba

datoteko pleb.bash damo v mapo v kateri imamo izvorno kodo (.c)
in zaženemo s "bash pleb.bash <ime programa(brez.c)> <mapa s testnimi primeri>".
Program bo zgeneriral spletno stran rezultati.html.

## Avtorja
Urban Suhadolnik: Program

Jure Vreček : Izgled spletne strani
