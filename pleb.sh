#!/bin/bash

# $1=ime programa
# $2=mapa s testnimi primeri

timeLimit=1;
stTestnihPrimerov=0;
stPass=0;
stfail=0;
#prazne spremenjivke
#kazalo
#telo


function passKazalo {
    kazalo+="<li class=\"passnav\"><a href=\"#test$ia$ib\">$ia$ib</a></li>";
}

function failKazalo {
    kazalo+="<li class=\"failnav\"><a href=\"#test$ia$ib\">$ia$ib</a></li>";
}

function overTimeKazalo {
    kazalo+="<li class=\"timeoutnav\"><a href=\"#test$ia$ib\">$ia$ib</a></li>";
}

function pass {
    let stPass=stPass+1
    zadnjiTest="pass";
    
    echo gooooouuud
    passKazalo
    #telo+="<p>testi$ia$ib je gooooouuuud</p>"
    
    telo+="<table id=\"test$ia$ib\" class=\"pass\">
			<tr>
				<th colspan=2>Testni primer $ia$ib</th>
			</tr>
			<tr>
				<td>Vhod</td>
				<td><pre>$(<test$ia$ib.in)</pre></td>
			</tr>
			<tr>
				<td>Izhod programa</td>
				<td><pre>$(<test$ia$ib.res)</pre></td>
			</tr>
		</table>
		<br />"
    
}
           

function fail {
    let stFail=stFail+1;
    zadnjiTest="fail";
    
    echo git good
    failKazalo
    telo+="<table id=\"test$ia$ib\" class=\"fail\">
			<tr>
				<th colspan=2>Testni primer $ia$ib</th>
			</tr>
			<tr>
				<td>Vhod</td>
				<td><pre>$(<test$ia$ib.in)</pre></td>
			</tr>
			<tr>
				<td>Izhod programa</td>
				<td><pre>$(<test$ia$ib.res)</pre></td>
			</tr>
			<tr>
				<td>Referenčni izhod</td>
				<td><pre>$(<test$ia$ib.out)</pre></td>
			</tr>
		</table>
		<br />"
		
}

function overTime {
    let stFail=stFail+1;
    zadnjiTest="timeout";
    
    echo too slow
    overTimeKazalo
    #telo+="<p>testi$ia$ib too slow</p>"
    
    telo+="<table id=\"test$ia$ib\" class=\"timeout\">
			<tr>
				<th colspan=2>Testni primer $ia$ib</th>
			</tr>
			<tr>
				<td>Vhod</td>
				<td><pre>$(<test$ia$ib.in)</pre></td>
			</tr>
			<tr>
				<td>Referenčni izhod</td>
				<td><pre>$(<test$ia$ib.out)</pre></td>
			</tr>
		</table>
		<br />"
		
}

#ZAČETEK PROGRAMA

#Preverjanje vhodnih parametrov
if [ -z "$1" ]|| [ -z "$2" ]; then
    echo "Sintaksa programa je $0 <ime programa> <mapa s testnimi primeri>"
    exit
fi
    

echo "Živjo svet" $1;

echo ====Compiling=====;
echo gcc -std=c99 -pedantic -Wall -o $1 $1.c -lm;
gcc -std=c99 -pedantic -Wall -o $1 $1.c -lm;


ia=0;
ib=1;

echo

cd $2
while [ -f test$ia$ib.in ]; do
    let stTestnihPrimerov=stTestnihPrimerov+1;
    zadnjiTest=fail; #spremenjivka za uspešnost testa
    
    
    echo test št. $ia$ib
    
    if (! timeout $timeLimit ../$1 < test$ia$ib.in > test$ia$ib.res); then #zagon programa s tesnim primerom 
        overTime;
    
    else
        diff --ignore-trailing-space test$ia$ib.res test$ia$ib.out > test$ia$ib.diff;
    
        if [ $? != 1 ]; then #$? je izhod zadnje fukncije
            pass
        else
            fail
        fi
    fi
    
    
    let ib=ib+1 #povečaj št testnega primera
    if [ $ib -eq 10 ]; then #če je več kot 9 povečej tudi prvo števko
        let ia=ia+1
        let ib=0
        
    fi
done

echo hello world;

#GENERATOR SPLETNE STRANI
#zaporedje strani:
#glava, naslovStrani, zKazala, kazalo, kKazala, telo, kstrani


glava='<!doctype html>
<html>
	<head>
		<title>Rezultati testiranja</title>
		<meta charset="UTF-8">
		<meta name="author" content="Urban Suhadolnik">
		<meta name="author" content="Jure Vreček">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <style type="text/css">
        body, pre {
            font-family: "Open Sans", sans-serif;
        }
        
        pre{
            margin:0;
        }

        h1 {
            display: flex;
            justify-content: center;
        }

        h2 {
            margin-left: 5%;
        }

        section {
            width: 90%;
        }

        table {
            margin-left: auto;
            margin-right: auto;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            border-collapse: collapse;
            width: 90%;
        }

        th {
            background-color: cornflowerblue;
            color: white;
        }

        tr {
            background-color: #e8effc;
        }

        td, th {
            border: 2px solid white;
            padding: 3px;
        }

        td:nth-child(odd) {
            font-weight: bold;
            width: 20%;
        }

        /*FAIL*/
        .fail {
            border-color: #e60000;
        }

        .fail tr {
            background-color: #ffe6e6;
        }

        .fail tr th {
            background-color: #e60000;
        }

        /*PASS*/
        .pass tr {
            background-color: #e6ffe6;
        }

        .pass tr th {
            background-color: #00cc00;
        }

        /*TIMEOUT*/
        .timeout tr {
            background-color: #f0ecf8;
        }

        .timeout tr th {
            background-color: #ac00e6;
        }

        /*NAV*/
        nav {
            width: 10%;
            position: fixed;
            right: 0;
            padding: 10px;
            border: 2px solid cornflowerblue;
            border-radius: 5px;
            margin-right: inherit;
        }

        nav h2 {
            margin-top: 0;
            text-align: center;
            margin-left: 0;
        }

        ul {
            list-style-type: none;
            padding-left: 10px;
            margin-left: auto;
            margin-right: auto;
        }

        li {
            display: inline-block;
            vertical-align: middle;
            line-height: normal;
            border: 2px solid cornflowerblue;
            border-radius: 3px;
            background-color: #e8effc;
            width: 2em;
            height: 1.5em;
            text-align: center;
        }
        
        nav a {
            text-decoration: none;
        }
        
        /*FAILNAV*/
        .failnav{
            background-color: #ffe6e6;
            border: 2px solid #cc0000;
        }
        
        .failnav a, .failnav a:visited{
            color: #cc0000;
        }
        
        /*PASSNAV*/
        .passnav{
            background-color: #e6ffe6;
            border: 2px solid #008000;
        }
        
        .passnav a, .passnav a:visited{
            color: #008000;
        }
        
        /*TIMEOUTNAV*/
        .timeoutnav{
            background-color: #f0ecf8;
            border: 2px solid #8600b3;
        }
        
        .timeoutnav a, .timeoutnav a:visited{
            color: #8600b3;
        }
    </style>
	</head>
	<body>';

naslovStrani="<h1>Rezultati preverjanja $1 ($stPass/$stTestnihPrimerov)</h1>"

zKazala="<nav>
         <h2>Kazalo</h2>
			<ul>";

#tukaj pride zgenerirano kazalo <li>

kKazala="</ul>
		</nav>

		<section>
		<h2>Rezultati</h2>";

#tukaj pridejo zgenerirani rezultati
		
		#"</table>
kStrani="
		</section>

	</body>
</html>";

#DEBUGG IZPIS
#echo ""
#echo $kazalo
#echo ""
#echo $telo
#echo ""
#echo $kStrani

cd .. #nazaj na originalno mapo

#izpis v datoteko
#zaporedje strani:
#glava, naslovStrani, zKazala, kazalo, kKazala, telo, kstrani


echo "$glava$naslovStrani$zKazala$kazalo$kKazala$telo$kStrani"  > rezultati.html

